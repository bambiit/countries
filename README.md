# Countries APIs

The project is for APIs development to get all countries information such as population, capital, flag, etc. from restcountries.eu. <br/>
This project is developed using Spring Boot with reactive programming in Java and circuit breaker. With microservice approach, a rate limiter should have been implemented in this project as well, however it is not available at the moment. <br/>
Moreover, the project is developed with inspiration of clean architecture from Uncle Bob.

## Getting started

To run the application 

```
./mvnw spring-boot:run 
```

## Apis Document

The api doc of the project is available at http://34.88.122.240/api-docs/

## CI/CD

The project can be continuously integration and deployment using Gitlab CI/CD. The process would be automatically triggered when a "push" action is operated to Gitlab. <br/> 
The deployment is implemented to Google Cloud that running Kubernetes. A project with a cluster need to be created as prerequisite on Google Cloud. <br/>
The K8s service created for this project is available with LoadBalancer type. This service is exposed externally in GCP, with their default protocol at port is TCP. <br/>

There are environment variables that need to be setup in Gitlab CI/CD environment variables beforehand.

### Environent Variables
```
GOOGlE_KEY: contain key information in json format that including credential information to run any commands on Google Cloud.
REGISTRY_EMAIL, REGISTRY_PASSWD, REGISTRY_USERNAME: Gitlab container registry credential information to create/push docker images to Gitlab Container Registry.
```

## Note
At the time this project is being developed, the service at https://restcountries.eu is currently out of service for some reason. Therefore, I cloned the github of restcountries project at https://github.com/apilayer/restcountries to local machine to fetch data for this project. <br/>
Therefore, some resources such as images url might not displayed correctly at the moment.  
