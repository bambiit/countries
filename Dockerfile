FROM adoptopenjdk/openjdk11:jdk-11.0.12_7-alpine
VOLUME /tmp
ADD /target/countries-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]